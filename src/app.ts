import * as dotenv from 'dotenv';
import express, { Application } from 'express';
import * as bodyParser from 'body-parser';
import { employeeRouter } from './routes/employee.routes';

const app: Application = express();
dotenv.config();

app.use(bodyParser.json());
app.use('/employee', employeeRouter);

export default app;
