export interface Employee {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  gender: string;
  photo: string;
}
