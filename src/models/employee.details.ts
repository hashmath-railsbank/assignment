import { db } from '../db';
import { OkPacket, RowDataPacket } from 'mysql2';
import { Employee } from '../types/employee';

export const findAllEmployee = (callback: Function) => {
  const queryString = `SELECT * FROM employee`;

  db.query(queryString, (err, result) => {
    if (err) {
      callback(err);
    }

    const rows = <RowDataPacket[]>result;
    const employees: Employee[] = [];

    rows.forEach((row) => {
      const employee: Employee = {
        id: row.id,
        firstName: row.first_name,
        lastName: row.last_name,
        email: row.email,
        mobileNumber: row.number,
        gender: row.gender,
        photo: row.photo
      };
      employees.push(employee);
    });
    callback(null, employees);
  });
};

export const findEmployee = async (employeeId: number, callback: Function) => {
  const queryString = `
      SELECT * FROM employee WHERE id=?`;

  db.query(queryString, employeeId, (err, result) => {
    if (err) {
      callback(err);
    }

    const row = (<RowDataPacket>result)[0];
    const employee: Employee = {
      id: row?.id,
      firstName: row?.first_name,
      lastName: row?.last_name,
      email: row?.email,
      mobileNumber: row?.number,
      gender: row?.gender,
      photo: row?.photo
    };

    callback(null, employee);
  });
};

export const createEmployee = (employee: Employee, callback: Function) => {
  const queryString =
    'INSERT INTO employee (first_name, last_name, email, number, gender, photo) VALUES (?, ?, ?, ?, ?, ?)';

  db.query(
    queryString,
    [
      employee.firstName,
      employee.lastName,
      employee.email,
      employee.mobileNumber,
      employee.gender,
      employee.photo
    ],
    (err, result) => {
      if (err) {
        callback(err);
      }

      const insertId = (<OkPacket>result).insertId;
      callback(null, insertId);
    }
  );
};

export const updateEmployee = (
  employee: Employee,
  employeeId: number,
  callback: Function
) => {
  const queryString = `UPDATE employee SET first_name=?, last_name=?, email=?, number=?, gender=?, photo=? WHERE id=?`;

  db.query(
    queryString,
    [
      employee.firstName,
      employee.lastName,
      employee.email,
      employee.mobileNumber,
      employee.gender,
      employee.photo,
      employeeId
    ],
    (err, result) => {
      if (err) {
        callback(err);
      }
      callback(null);
    }
  );
};

export const deleteEmployee = (employeeId: number, callback: Function) => {
  const queryString = `DELETE FROM employee WHERE id=?`;

  db.query(queryString, [employeeId], (err, result) => {
    if (err) {
      callback(err);
    }
    callback(null);
  });
};
