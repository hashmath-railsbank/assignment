import express, { Request, Response, Router } from 'express';
import * as employeeModel from '../models/employee.details';
import { Employee } from '../types/employee';

const employeeRouter = Router();

employeeRouter.get('/', async (req: Request, res: Response) => {
  employeeModel.findAllEmployee((err: Error, employees: Employee[]) => {
    if (err) {
      return res.status(500).json({ errorMessage: err.message });
    }
    res.status(200).json({ employees });
  });
});

employeeRouter.get('/:id', async (req: Request, res: Response) => {
  const employeeId: number = Number(req.params.id);
  employeeModel.findEmployee(employeeId, (err: Error, employee: Employee) => {
    if (err) {
      return res.status(500).json({ message: err.message });
    }
    if (employee.id) {
      res.status(200).json({ employee });
    } else {
      const error = {
        statusCode: 404,
        message: 'No employee found for the given employee id',
        employeeId: employeeId
      };
      res.status(404).json({ error });
    }
  });
});

employeeRouter.post('/', async (req: Request, res: Response) => {
  const newEmployee: Employee = req.body;
  employeeModel.createEmployee(
    newEmployee,
    (err: Error, employeeId: number) => {
      if (err) {
        return res.status(500).json({ message: err.message });
      }

      res.status(200).json({ employeeId: employeeId });
    }
  );
});

employeeRouter.put('/:id', async (req: Request, res: Response) => {
  const employee: Employee = req.body;
  const employeeId: number = Number(req.params.id);
  employeeModel.updateEmployee(employee, employeeId, (err: Error) => {
    if (err) {
      return res.status(500).json({ message: err.message });
    }

    res.status(200).send();
  });
});

employeeRouter.delete('/:id', async (req: Request, res: Response) => {
  const employeeId: number = Number(req.params.id);
  employeeModel.deleteEmployee(employeeId, (err: Error) => {
    if (err) {
      return res.status(500).json({ message: err.message });
    }

    res.status(200).send();
  });
});

export { employeeRouter };
