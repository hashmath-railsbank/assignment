import app from './app';

app.listen(process.env.PORT, () => {
  console.log(`Node server started running on port ${process.env.PORT}`);
});
