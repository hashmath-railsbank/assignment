import request from 'supertest';

import app from '../../src/app';
import { Employee } from '../../src/types/employee';

describe('Employee routes', () => {
  let employeeId: number;

  test('Get all employees', async () => {
    const res = await request(app).get('/employee');
    expect(res.status).toEqual(200);
    expect(res.status).toMatchSnapshot();
  });

  test('Add new employees', async () => {
    const res = await request(app).post('/employee').send({
      firstName: 'John',
      lastName: 'Cena',
      email: 'john.cena@yahoo.com',
      mobileNumber: '+94771277121',
      gender: 'M',
      photo: 'https://randomuser.me/api/portraits/men/34.jpg'
    });
    employeeId = JSON.parse(res.text).employeeId;
    expect(res.status).toEqual(200);
    expect(res.status).toMatchSnapshot();
  });

  test('Get employee by id', async () => {
    const res = await request(app).get(`/employee/${employeeId}`);
    expect(JSON.parse(res.text).employee.firstName).toEqual('John');
    expect(JSON.parse(res.text).employee.lastName).toEqual('Cena');
    expect(JSON.parse(res.text).employee.email).toEqual('john.cena@yahoo.com');
    const employee = JSON.parse(res.text);
    delete employee['employee'].id;
    expect(employee).toMatchSnapshot();
  });

  test('Update employee by id', async () => {
    const res = await request(app).put(`/employee/${employeeId}`).send({
      firstName: 'John',
      lastName: 'Carter',
      email: 'john.carter@yahoo.com',
      mobileNumber: '+94771277121',
      gender: 'M',
      photo: 'https://randomuser.me/api/portraits/men/34.jpg'
    });
    expect(res.status).toEqual(200);
    expect(res.status).toMatchSnapshot();
  });

  test('Delete employee by id', async () => {
    const res = await request(app).delete(`/employee/${employeeId}`);
    expect(res.status).toEqual(200);
    expect(res.status).toMatchSnapshot();
  });
});
