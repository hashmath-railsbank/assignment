## To Build the project
```shell script
npm run build
```
## To Run the project
```shell script
npm run start
```

## To Test the project
```shell script
npm run test
```